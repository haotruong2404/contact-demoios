//
//  APIController.swift
//  demo
//
//  Created by VHao on 11/16/20.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit

class APIController: NSObject {
//MARK: - APIs
     
    private let baseURL = "https://5fbfbd9efd14be0016749054.mockapi.io/apihao/v1/contacts1"
    static let shared = APIController()  //Singleton
    
    //GET
    func getAvatar(urlString: String, completion: @escaping (UIImage?) ->()) {
        guard let url = URL(string: urlString) else {
            completion(nil)
            return
        }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
            completion(nil)
            }
            guard let data = data, let image = UIImage(data: data) else {
                completion(nil)
                return
            }
            completion(image)
            }
        task.resume()
}
    
    func fetchContacts(completion: @escaping( [ContactModel]?, Error?) -> ()) {
        if let url = URL(string: baseURL) {
            let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
                if let error = error {
                    completion(nil,error)
                    return
                }
                if let data = data, let list = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] {
                    var contactlist = [ContactModel]()
                    list.forEach { (dict) in
                        let contact = ContactModel (dictionary:dict)
                        contactlist.append(contact)
                    }
                    completion(contactlist,nil)
            }
        }
            task.resume()
        } else {
        completion(nil,createError(message: "Loi URL"))
        }
    }
    
    //DELETE
    func deleteContact(idContact: String, completion: @escaping (Error?) -> ()) {
        
        let urlString = baseURL + "/" + idContact
        guard let url = URL(string: urlString) else {
            completion(createError(message: "Lỗi URL"))
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "DELETE"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                completion(error)
                return
            }
            if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                completion(nil)
                return
            }
            completion(self.createError(message: "Lỗi khi xoá Danh bạ"))
        }
        task.resume()
    }
    //POST
    func postContact(with contact: ContactModel, completion: @escaping(ContactModel?, Error?) -> ()) {
        guard let url = URL(string: baseURL) else {
            completion(nil,createError(message: "Lỗi URL"))
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        guard let data = try? JSONSerialization.data(withJSONObject: contact.toDictionary(), options: []),
        let jsonString = String (data: data, encoding: .utf8),
        let dataRequest = jsonString.data(using: .utf8) else {
            completion(nil,createError(message: "Lỗi khi thêm danh bạ"))
            return
        }
        
        urlRequest.httpBody = dataRequest
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                completion(nil,error)
                return
            }
            if let data = data {
                if let resutl = try? JSONSerialization.jsonObject(with: data, options: []) as?  [String: Any] {
                    let contact = ContactModel(dictionary: resutl)
                    completion(contact,nil)
                    return
                }
            }
            
            completion(nil,self.createError(message: "Lỗi khi xoá Danh bạ"))
        }
        task.resume()
    }
    
//    func fetchContacts(completion: @escaping( Result<[ContactModel], Error>) -> ()) {// buoc 1:tao url
//        guard let url = URL(string: baseURL) else {
//            //truong hop url loi
//            completion(.failure(createError(message: "Lỗi URL")))
//            return
//        }
//        //b2: tao URLSession
//        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
//            if let error = error{
//                completion(.failure(error))
//            }
//            //b3: chuyen du lieu tu server ve du lieu ma chung ta muon
//            guard let data = data, let list = try? JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]] else {
//                completion(.failure(self.createError(message: "Lỗi khi lấy danh ba")))
//                return
//            }
//            //Thanh cong
//            var contactList = [ContactModel] ()
//
//            list.forEach { (dict) in
//                let contact = ContactModel.init(dictionary: dict)
//                contactList.append(contact)
//            }
//            completion(.success(contactList))
//        }
//        task.resume() // bat dau thuc hien
//}
    
//MARK: - Helper
    private func createError(message: String? = nil) -> NSError {
        return NSError(domain: "com.contacts", code: 400, userInfo: [NSLocalizedRecoveryOptionsErrorKey: message ?? "Đã có lỗi xảy ra"])
    }
}
