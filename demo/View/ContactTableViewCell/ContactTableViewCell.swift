//
//  ContactTableViewCell.swift
//  demo
//
//  Created by Khai on 01/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit
import SDWebImage

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    
    static let cellIdentifier: String = "ContactTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        shadowView.layer.shadowRadius = 5
//        shadowView.layer.shadowColor = UIColor.gray.cgColor
//        shadowView.layer.shadowOpacity = 1
//        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
//        shadowView.layer.cornerRadius = 20
        
        avatarImageView.layer.cornerRadius = 20
        avatarImageView.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        avatarImageView.image = nil
    }
    
    func setupCell(contactModel: ContactModel) {
        nameLabel.text = contactModel.name  //firstName! + " " + contactModel.lastName!
        phoneLabel.text = contactModel.phone
        
        if let avatar = contactModel.avatar, avatar != "", let url = URL(string: avatar) {
            // getAvatar(urlString: avatar) dùng với hàm getAvatar
            avatarImageView.sd_setImage(with: url, completed: nil)
        }
    }
    
//    func getAvatar(urlString: String) {
//        APIController.shared.getAvatar(urlString: urlString) {[weak self] (avatar) in
//            if let avatar = avatar {
//                DispatchQueue.main.async {
//                    self?.avatarImageView.image = avatar
//                }
//            }
//        }
//    }
    
    @IBAction func didTapCallButton(_ sender: Any) {
        
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "ContactTableViewCell", bundle: nil)
    }
}
