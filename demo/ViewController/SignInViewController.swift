//
//  SignInViewController.swift
//  demo
//
//  Created by Khai on 10/28/20.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit
import FirebaseAuth
import MBProgressHUD

class SignInViewController: ViewController {
    
    @IBOutlet weak var PassWordView: UIView!
    @IBOutlet weak var ParentPassWord: UIView!
    

    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var welComeBackLabel: UILabel!
    //@IBOutlet weak var signInButton: UIButton!//Khai bao bien
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var parentUserName: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    
    private var isLogin = true //cờ để check điều kiện
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    func setupUI() // thiết lập giao diện
    {
        avatarImageView.layer.cornerRadius = 70
        parentUserName.layer.cornerRadius = 15
        userNameView.layer.cornerRadius = 15
        ParentPassWord.layer.cornerRadius = 15
        PassWordView.layer.cornerRadius = 15
        signInButton.layer.cornerRadius = 15
        registerButton.layer.cornerRadius = 4
        
        //self.navigationItem.title = "Sign In Screen"
        //welComeBackLabel.text = "Hello world"
    }
    
    private func showHomeScreen () {
        let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        let contactViewController = mainStoryboard.instantiateViewController(withIdentifier: "ContactViewController")
        self.navigationController?.pushViewController(contactViewController, animated: true)
    }
    
    private func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    //    func validateSignIn(){//Kiểm tra dữ liệu đăng nhập
    //        if let userName = userNameTextField.text , let passWord = passWordTextField.text ,
    //            userName != "",
    //            passWord != "",
    //            passWord.count >= 6
    //        {
    //            print("Validate success: " + userName + "||" + passWord)
    //        }else{
    //            print("Validate fail")
    //        }
    //   }
    
    //MARK: - Action
    @IBAction func didClickSignInButton(_ sender: Any) {//login tai khoan  voi du lieu ma nguoi dung nhap vao
        //validateSignIn()
        //B1 Tạo ra 1 storyboard  chưa viewcontroller muốn hiển thị(Main.storyboard)
        
        //            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
        ////        //B2 Tạo ra viewcontroller muốn hiển thị(ContactViewController)
        ////
        //            let contactViewController = mainStoryboard.instantiateViewController(withIdentifier: "ContactViewController")
        ////
        ////        //B3:Thực hiện hiển thị viewController(ContactViewController)
        ////        self.present(contactViewController, animated: true, completion: nil)
        //
        //            self.navigationController?.pushViewController(contactViewController, animated: true)
        guard let email = userNameTextField.text, let password = passWordTextField.text, email != "" , password != "" else {
            print( "Email or Password lỗi")
            return
        }
        
        guard isValidEmail(email) else {
            print("Email không đúng định dạng")
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        if isLogin {
           // showHomeScreen()
            Auth.auth().signIn(withEmail: email, password: password) { [weak self] (result, error) in
                guard let self = self else { return }
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result = result {
                    print(result)
                    print("Đăng nhập thành công")
                    self.showHomeScreen()
                } else {
                    print("Có lỗi khi đăng nhập \(error?.localizedDescription)")
                }
            }
        } else {
            Auth.auth().createUser(withEmail: email, password: password) { [weak self] (result, error) in
                guard let self = self else { return }
                MBProgressHUD.hide(for: self.view, animated: true)
                if let result = result {
                    print(result)
                    print("Tao taị khoản thành công")
                    self.showHomeScreen()
                } else {
                    print("Có lỗi khi tạo tại khoản \(error?.localizedDescription)")
                }
            }
            //print("Register")
        }
    }
    @IBAction func didClickRegisterButton(_ sender: Any) {
        signInButton.setTitle(isLogin ? "REGISTER" : "LOGIN" , for: .normal)
        registerButton.setTitle(isLogin ? "Login" : "Register", for: .normal)
        
        isLogin = !isLogin //đảo ngược giá trị của islogin
//        if isLogin {
//            signInButton.setTitle("REGISTER", for: .normal)
//            registerButton.setTitle("Log In", for: .normal)
//        } else {
//            signInButton.setTitle("LOG IN", for: .normal)
//            registerButton.setTitle("Register", for: .normal)
//        }
    }
}

//        if userNameTextField != nil{
//            print(userNameTextField.text!)
//        }
//        if passWordTextField != nil{
//            print(passWordTextField.text!)
//        }
//        if userNameTextField.text != nil &&                       passWordTextField != nil {





//
//        }
//        if let userName =  userNameTextField.text{
//            print(userName)
//        }
//        if let passWord = passWordTextField.text{
//            print(passWord)
//        }
//        if let userName = userNameTextField.text,let passWord =    passWordTextField.text{
//            print(userName + "||" + passWord )
