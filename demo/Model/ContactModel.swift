//
//  ContactModel.swift
//  demo
//
//  Created by Khai on 02/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit

class ContactModel: NSObject {
    var name : String {
        return (firstName ?? "") + " " + (lastName ?? "")
    }
    var phone : String?
    var avatar : String?
    var mail: String?
    var address: String?
    var firstName : String?
    var lastName : String?
    var id : String?
    var job : String?
    
    init(firstName: String, lastName: String, phone: String, avatar: String, mail: String, address:String, job:String ) {//Khởi tạo một đối tượng mới
        self.firstName = firstName
        self.lastName = lastName
        self.phone = phone
        self.avatar = avatar
        self.mail = mail
        self.address = address
        self.job = job
    }
    
    init(dictionary:[String: Any]) {
        if let id = dictionary["id"] as? String {
            self.id = id
        }
        if let firstName = dictionary["firstName"] as? String {
            self.firstName = firstName
        }
        if let lastName = dictionary["lastName"] as? String {
            self.lastName = lastName
        }
        if let mail = dictionary["mail"] as? String {
            self.mail = mail
        }
        if let phone = dictionary["phone"] as? String {
            self.phone = phone
        }
        if let avatar = dictionary["avatar"] as? String {
        self.avatar = avatar
        }
        if let job = dictionary["job"] as? String {
        self.job = job
        }
        if let address = dictionary["address"] as? String {
        self.address = address
        }
    }
    
    func toDictionary () -> [String: Any] {
        var dictionary = [String:Any] ()
        if let firstName = firstName {
            dictionary["firstName"] = firstName
        }
        if let lastName = lastName {
            dictionary["lastName"] = lastName
        }
        if let phone = phone {
            dictionary["phone"] = phone
        }
        if let mail = mail {
            dictionary["mail"] = mail
        }
        if let address = address {
            dictionary["address"] = address
        }
        if let job = job {
            dictionary["job"] = job
        }
        return dictionary
    }
}
