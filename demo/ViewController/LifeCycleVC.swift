//
//  LifeCycleVC.swift
//  demo
//
//  Created by Khai on 04/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit

class LifeCycleVC: UIViewController {
    //Được gọi đầu tiên khi view được khởi tạo
    override func viewDidLoad() {//Chỉ chạy 1 lần duy nhất sau khi được khởi tạo
        super.viewDidLoad()
        print("viewDidLoad")
    }
    //Được gọi view start khi được hiển thị
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
    }
    //Đã hiểu thị view khi hiển thị xong
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
    }
    //Được gọi khi bắt đầu chuyển sang chế độ xem khác
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
    }
    //Được gọi sau khi chuyển sang chế độ xem khác
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print("viewDidDisappear")
    }
}
