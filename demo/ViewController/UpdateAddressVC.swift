//
//  UpdateAddressVC.swift
//  demo
//
//  Created by Khai on 08/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit

class UpdateAddressVC: UIViewController {

    @IBOutlet weak var addressTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didClickUpdateAddress(_ sender: Any) {
        //B1: gui di thong bao
        guard let address = addressTF.text else {
            return
        }
        //Cach 1: dung Object
//        NotificationCenter.default.post(name: NSNotification.Name("didClickUpdateAddress"), object: addressTF.text)
        //Cach 2: Dung userInfo
         NotificationCenter.default.post(name: NSNotification.Name("didClickUpdateAddress"), object: nil, userInfo: ["address": address])
    }
}
