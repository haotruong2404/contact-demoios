//
//  Contact1TableViewCell.swift
//  demo
//
//  Created by Khai on 03/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit

class Contact1TableViewCell: UITableViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setupCell(name:String?, avatar: String?){
        nameLabel.text = name
        if let avatar = avatar{
            avatarImageView.image = UIImage(named: avatar)
        }
    }
}
