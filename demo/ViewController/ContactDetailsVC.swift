//
//  ContactDetailsVC.swift
//  demo
//
//  Created by Khai on 05/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit
import SDWebImage

class ContactDetailsVC: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var contactModel: ContactModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDetail()
        
    }
    
    private func setupDetail () {
        self.navigationItem.title = contactModel?.name
        
        let backBarButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .done, target: self, action: #selector(didClickBackBarButton))
        self.navigationItem.leftBarButtonItem = backBarButton
        
        nameLabel.text = contactModel!.firstName! + " " + contactModel!.lastName!
        phoneLabel.text = contactModel?.phone
        addressLabel.text = contactModel?.address
        mailLabel.text = contactModel?.mail
        jobLabel.text = contactModel?.job
        
//        if let avatar = contactModel?.avatar, avatar != "", let url = URL(string: avatar) {
//            //getAvatar(urlString: avatar)
//        }
        
        //contentview
        contentView.layer.shadowColor = UIColor.orange.cgColor
        contentView.layer.shadowRadius = 7
        contentView.layer.shadowOpacity = 1
        contentView.layer.shadowOffset = CGSize(width: 0, height: 0)
        contentView.layer.cornerRadius = 12
        
        //avatar view
        avatarView.layer.shadowColor = UIColor.orange.cgColor
        avatarView.layer.shadowOffset = CGSize(width: 0, height: 0)
        avatarView.layer.shadowOpacity = 1
        avatarView.layer.shadowRadius = 8
        avatarView.layer.cornerRadius = avatarImageView.frame.width/2

        avatarImageView.layer.cornerRadius = avatarImageView.frame.width/2
        
        DispatchQueue.global().async {
            if let url = URL(string: self.contactModel?.avatar ?? "") { //, let data = try? Data(contentsOf: url) {
            DispatchQueue.main.async {
                   // self.avatarImageView.image = UIImage(data: data)
                    self.avatarImageView.sd_setImage(with: url, completed: nil)
                }
            }
        }
         
//        avatarImageView.sd_setImage(with: url, completed: nil)
                    //getAvatar(urlString: avatar)

        
        
    }
    
    

       

//Get Avatar
//    private func getAvatar (urlString: String) {
//        APIController.shared.getAvatar(urlString: urlString) { [weak self] (avatar) in
//            // guard let self = self else { return }
//            if let avatar = avatar {
//                DispatchQueue.main.async {
//                    self?.avatarImageView.image = avatar
//                }
//            }
//        }
//    }

    
    @objc func didClickBackBarButton(){
        self.navigationController?.popViewController(animated: true)
    }
}
