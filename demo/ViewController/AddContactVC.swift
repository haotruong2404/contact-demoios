//
//  AddContactVC.swift
//  demo
//
//  Created by Khai on 06/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit




//B1
protocol AddContactVCDelegate: class {
    func didFinishAddContact(contactModel: ContactModel)
}

class AddContactVC: UIViewController {
    
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var mailTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var jobTF: UITextField!
    
    //B1 Tao closure
    var didClickAddContact: ((_ contact: ContactModel) -> Void)?
    
    //B2
    weak var delegate:AddContactVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let addBarButton = UIBarButtonItem(image: UIImage(named: "ic_plus"), style: .done, target: self, action: #selector(didClickAddBarButton))
        self.navigationItem.rightBarButtonItem = addBarButton
    }
    
    @objc func didClickAddBarButton() {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "UpdateAddressVC") {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func didClickAddButton(_ sender: Any) {
        if let firstName = firstNameTF.text,
           let lastName = lastNameTF.text,
           let phone = phoneTF.text,
           let mail = mailTF.text,
           let address = addressTF.text,
           let job = jobTF.text
           {
            let avatar =  "ic_user"
           let contactModel = ContactModel(firstName: firstName, lastName: lastName, phone: phone, avatar: avatar, mail: mail, address: address, job: job)
            //b3 gui su kien di
          addContact(with: contactModel)
           
        }
   }
    
    //MARK: - APIs
    
    func addContact(with contact: ContactModel) {
        APIController.shared.postContact(with: contact) { [weak self] (contact,error) in
            guard let self = self else { return }
            if let error = error {
                print(error.localizedDescription)
            }
            if let contact = contact{
                DispatchQueue.main.async {
                self.didClickAddContact?(contact)
                self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

