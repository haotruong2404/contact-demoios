//
//  ContactViewController.swift
//  demo
//
//  Created by Khai on 01/11/2020.
//  Copyright © 2020 Khai. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import FirebaseAuth

class ContactViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var contactList = [ContactModel]()
    private var contactListRoot = [ContactModel]()
    private var indicatorView : NVActivityIndicatorView!
    
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // print("email \(Auth.auth().currentUser?.email)") //có email khi đăng nhập
        
        //TableView
        tableView.register(ContactTableViewCell.nib(), forCellReuseIdentifier: ContactTableViewCell.cellIdentifier)
        
        let signOutBarButton = UIBarButtonItem(image: UIImage(named: "ic_signout"), style: .done, target: self, action: #selector(didClickSignOutBarButton))
        self.navigationItem.leftBarButtonItem = signOutBarButton
        
        let addBarButton = UIBarButtonItem(image: UIImage(named: "ic_plus"), style: .done, target: self, action: #selector(didClickAddBarButton))
        self.navigationItem.rightBarButtonItem = addBarButton
        
        //indicatorVIew
        let width:CGFloat = 50
        let frame = CGRect(x: (UIScreen.main.bounds.width - width) / 2, y: (UIScreen.main.bounds.height - width) / 2, width:width, height: width)
        indicatorView = NVActivityIndicatorView(frame: frame, type: .lineSpinFadeLoader, color: .gray, padding: 0)
        //thêm indicatorView vào view
        self.view.addSubview(indicatorView)
        
        fetchContacts()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    //MARK: -Helps
    
    @objc func didClickSignOutBarButton() {
        do {
            try Auth.auth().signOut()
            showLoginScreen()
          //  print(Auth.auth().currentUser?.email) // sau khi sign out thì email = nil
        } catch let error {
            print("Lỗi khi đăng xuất \(error.localizedDescription)")
        }
        
        //self.navigationController?.popViewController(animated: true) //show lại màn hình trc đó
    }
    
    private func showLoginScreen () {
        if let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") {
            loginVC.modalPresentationStyle = .fullScreen
            self.present(loginVC, animated: false, completion: nil)
        }
    }
    
    @objc func didClickAddBarButton() {
        let addContactVC = self.storyboard?.instantiateViewController(withIdentifier: "AddContactVC") as! AddContactVC
        //B4 Dang ki
        // addContactVC.delegate = self
        
        addContactVC.didClickAddContact = { contact in
            self.contactList.append(contact)
            self.contactListRoot.append(contact)
            self.tableView.reloadData()
        }
        self.navigationController?.pushViewController(addContactVC, animated: true)
        
    }
    
    private func deleteCotactFromLocal(_ contact : ContactModel) {
        if let index = contactListRoot.firstIndex(of: contact) {
            contactListRoot.remove(at: index)
        }
        if let index = contactList.firstIndex(of: contact) {
            contactList.remove(at: index)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    //MARK: - Action
    @IBAction func didClickBackButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: -APIs
    
    private func deleteContact(_ contact: ContactModel) {
        guard let idContact = contact.id else {
            return
        }
        
        APIController.shared.deleteContact(idContact: idContact) {
            [weak self] (error) in
            guard let self = self else { return }
            if let error = error {
                print(error.localizedDescription)
            } else {
                //xoá thành công
                self.deleteCotactFromLocal(contact)
            }
        }
    }
    private func fetchContacts() {
        indicatorView.startAnimating()
        APIController.shared.fetchContacts {[weak self] (contactList, error) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.indicatorView.stopAnimating()
            }
            //ktra co loi hay khong
            if let error = error {
                print(error.localizedDescription)
            }
            if let contactList = contactList {
                // gán dữ liệu
                self.contactList = contactList
                self.contactListRoot = contactList
                DispatchQueue.main.async { // tính bất đồng bộ
                    // update UI
                    self.tableView.reloadData()
                }
            }
        }
    }
}
//func fetchContacts() {
//    APIController.shared.fetchContacts { (result), in switch result {
//    case .failure(let error):
//        print("Error \(error.localizedDescription)")
//        let alert = UIAlertController(title: "Thông báo", message: "Đã có lỗi. Vui lòng thử lại sau", preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
//        alert.addAction(okAction)
//        DispatchQueue.main.async {
//            self.present(alert, animated: true, completion: nil)
//        }
//        break
//    case .success(let contactList):
//        self.contactList = contactList
//       // self.contactList = self.contactListRoot
//        DispatchQueue.main.async {
//            self.tableView.reloadData()
//        }
//        break
//        }
//
//    }


//Thiết lập tableView
//MARK: -TableView
extension ContactViewController: UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let contactModel = contactList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell",for: indexPath) as! ContactTableViewCell
        cell.setupCell(contactModel: contactModel)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let contactModel = contactList[indexPath.row]
        if let contactDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "ContactDetailsVC") as? ContactDetailsVC{
            contactDetailsVC.contactModel = contactModel
            self.navigationController?.pushViewController(contactDetailsVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let contact = contactList[indexPath.row]
        
        let popup = UIAlertController(title: "Thông báo", message: "Bạn có muốn xoá \(contact.name) hay không", preferredStyle: .alert)
        
        let noAction = UIAlertAction(title: "Không", style: .cancel) { action in
            
        }
        
        let yesAction = UIAlertAction(title: "Có", style: .destructive) { [weak self] action in
            guard let self = self else { return }
            self.deleteContact(contact)
        }
        popup.addAction(noAction)
        popup.addAction(yesAction)
        self.present(popup, animated: true, completion: nil)
    }
}

//Bước 5
//MARK: -AddContactVCDelegate
extension ContactViewController: AddContactVCDelegate{
    func didFinishAddContact(contactModel: ContactModel) {
        contactList.append(contactModel)
        //lam moi du lieu tableview
        tableView.reloadData()
    }
}

//MARK: -SearchBar Delegate

extension ContactViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            contactList = contactListRoot
            tableView.reloadData()
            searchBar.resignFirstResponder()
            return
        }
        let result = contactListRoot.filter { (contact) -> Bool in
            return contact.name.lowercased().contains(searchText.lowercased()) 
        }
        contactList = result
        tableView.reloadData()
    }
}






////MARK: - Notification
//extension ContactViewController{
//    //B2 Dang ki
//    func registerObserver(){
//        NotificationCenter.default.addObserver(self, selector: #selector(didUpdateAddress(data:)), name: NSNotification.Name("didClickUpdateAddress"), object: nil)
//    }
//    @objc func didUpdateAddress(data: Notification){
//
//        //C1:Dung object
////        if let address = data.object as? String{
////            addressLabel.text = address
//        //C2: Dung userInfo
//        if let userInfo = data.userInfo,let address = userInfo["address"] as? String{
//            addressLabel.text = address
//        }
//      }
//
//    //B3: Huy dang ki
//    func removeObserver() {
//        NotificationCenter.default.removeObserver(self)
//        //          NotificationCenter.default.removeObserver(self, name: didClickUpdateAddress, object: nil)
//    }
//}
